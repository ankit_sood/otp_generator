package com.spring.otp.otp_generator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;


@EnableAutoConfiguration(exclude = {SecurityAutoConfiguration.class})//to bypass this spring boot default security mechanism.
@SpringBootApplication
public class OtpGeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(OtpGeneratorApplication.class, args);
	}
}
