package com.spring.otp.otp_generator.service;

import java.util.Arrays;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.spring.otp.otp_generator.model.User;
import com.spring.otp.otp_generator.repo.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return userRepository.findById(username).map(user ->{
			GrantedAuthority authority = new SimpleGrantedAuthority(user.getRole());
			UserDetails userDetails = (UserDetails) new org.springframework.security.core.userdetails.User(user.getUsername(), 
					user.getPassword(), Arrays.asList(authority));
			return userDetails;
		})
		.orElseThrow(() -> new UsernameNotFoundException("No user found with username: "+username));
	}
	
	public User getEmailId(String userName) {
		return userRepository.findById(userName).orElse(null);
	}
	
	public User addUser(User user) {
		return userRepository.saveAndFlush(user);
	}

}
