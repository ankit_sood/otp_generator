package com.spring.otp.otp_generator.controller;

import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.spring.otp.otp_generator.model.User;
import com.spring.otp.otp_generator.service.EmailService;
import com.spring.otp.otp_generator.service.OtpService;
import com.spring.otp.otp_generator.service.UserDetailsServiceImpl;
import com.spring.otp.otp_generator.utility.EmailTemplate;

@RestController
public class OtpController {
	
	private final OtpService otpService;
	private final EmailService emailService;
	private final BCryptPasswordEncoder passwordEncoder;
	private final UserDetailsServiceImpl userDetailService; 
	
	public OtpController(OtpService otpService,EmailService emailService,UserDetailsServiceImpl userDetailsService,BCryptPasswordEncoder passwordEncoder) {
		this.otpService = otpService;
		this.emailService = emailService;
		this.userDetailService = userDetailsService;
		this.passwordEncoder = passwordEncoder;
	}
	
	@GetMapping("/generateOTP")
	public boolean getOTP() throws MessagingException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName();
		int otp = otpService.generateOTP(username);
		User user = userDetailService.getEmailId(username);
		//Generating the Template to send the OTP.
		EmailTemplate template = new EmailTemplate("OtpMessage.txt");
		Map<String,String> replacements = new HashMap<String,String>();
		replacements.put("user", user.getFullName());
		replacements.put("otpnum", String.valueOf(otp));
		String message = template.getTemplate(replacements);
		emailService.sendOtpMessage(user.getEmail(), "One Time Password", message);
		
		return true;
	}
	
	@GetMapping("/validateOTP")
	public boolean validateOTP(@RequestParam Integer otpNum) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName();
		
		if(otpNum!=null && otpNum>0) {
			int serverOTP = otpService.getOTP(username);
			if(serverOTP>0 && otpNum.intValue()==serverOTP) {
				return true;
			}
			
		}
		return false;
	}
	
	@PostMapping("/add/user")
	public String addUser(@RequestBody User user) {
		if(!StringUtils.isEmpty(user.getUsername()) && !StringUtils.isEmpty(user.getPassword())) {
			user.setPassword(passwordEncoder.encode(user.getPassword()));
			userDetailService.addUser(user);
			return "SUCCESS";
		}else {
			return "FAILURE";
		}
	}
}
