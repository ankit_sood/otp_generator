package com.spring.otp.otp_generator.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.spring.otp.otp_generator.model.User;

public interface UserRepository extends JpaRepository<User,String>{
	
}
