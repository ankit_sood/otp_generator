# Welcome to OTP Generator

##Introduction
This is a sample project which is used to demonstrate the working of a program which generates the One time password.
It consists of three endpoints, which are listed below:
-	/generateOTP
-	/validateOTP
- 	/add/user

Here, endpoints to generate and validate OTPs are secured for the demo purpose. Functionality of these endpoints are self explanatory.

##Testing
In order to test the endpoints please use the data given below:
-	Request Method: POST, URL: http://localhost:8081/add/user with the following httpHeaders and request body:    
	Content-Type: application/json (Free from authentication)    
	Request Body: {    
		  				"username": "soodank",   
			  			"password": "test",   
				  		"role": "USER",     
				  		"fullName": "ANKIT SOOD",     
			   			"country": "INDIA",     
			   			"email": "soodankit1993@gmail.com"   
				  }
				     
-	Request Method: GET, URL: http://localhost:8081/validateOTP?otpNum=<GeneratedValue> with the following httpHeaders:         	
	Content-Type: application/json and BasicAuth Username:soodank	Passwaord: test     
	    
-	Request Method: GET, URL: http://localhost:8081/generateOTP with the following httpHeaders: 	    
	Content-Type: application/json and BasicAuth Username:soodank	Passwaord: test (You can always create your own user with the endpoint mentioned above and use it)       