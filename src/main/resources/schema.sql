DROP TABLE IF EXISTS User;

CREATE TABLE `User` (
  `Username` VARCHAR(256) NOT NULL,
  `Password` VARCHAR(256) NOT NULL,
  `Role` VARCHAR(45) NOT NULL,
  `FullName` VARCHAR(45) NOT NULL,
  `Country` VARCHAR(45) NOT NULL,
  `Email` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Username`),
  UNIQUE INDEX `Username_UNIQUE` (`Username` ASC));