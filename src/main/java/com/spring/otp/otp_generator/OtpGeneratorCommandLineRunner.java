package com.spring.otp.otp_generator;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.spring.otp.otp_generator.model.User;
import com.spring.otp.otp_generator.repo.UserRepository;

@Component
public class OtpGeneratorCommandLineRunner implements CommandLineRunner{
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BCryptPasswordEncoder bcryptEncoder;
	@Override
	public void run(String... args) throws Exception {
		Optional<User> userOp = userRepository.findById("root");
		if(!userOp.isPresent()){
			User user = new User();
			user.setUsername("root");
			user.setPassword(bcryptEncoder.encode("root"));
			user.setCountry("INDIA");
			user.setEmail("soodankit1993@gmail.com");
			user.setFullName("ADMIN");
			user.setRole("ADMIN");
			userRepository.save(user);
		}
	}
}
